//
// Created by Adrian Karlsen on 21/03/2021.
//

#ifndef C_FALCONUTILS_HASHMAP_H
#define C_FALCONUTILS_HASHMAP_H

#endif //C_FALCONUTILS_HASHMAP_H

#include "../../Utils.h"
#include "./HashMap/Hash.h"


typedef struct Class_HashMap HashMap;

HashMap* HASHMAP_new(size_t elementSize);

void HASHMAP_init(HashMap * map, size_t valueTypeSize);

void HASHMAP_free(HashMap* map);

void HASHMAP_add(HashMap* map, char* key, void* element);

void HASHMAP_add_useStringKey(HashMap* map, String* key, void* element);

void HASHMAP_remove(HashMap* map, char* key);

void HASHMAP_remove_useStringKey(HashMap* map, String* key);

size_t HASHMAP_getElmSize(HashMap* map);

void* HASHMAP_get(HashMap* map, char* key);

void* HASHMAP_get_useStringKey(HashMap* map, String* key);

void* HASHMAP_set(HashMap* map, char* key, void* elem);

void* HASHMAP_set_useStringKey(HashMap* map, String* key, void* elem);

Array HASHMAP_getValues(HashMap* map);

Array HASHMAP_getKeys(HashMap* map);

size_t HASHMAP_getSizeOf();

int HASHMAP_getKeyHash(String* key);