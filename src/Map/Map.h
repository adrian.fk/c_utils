//
// Created by Adrian Karlsen on 31/01/2021.
//

#ifndef C_FALCONUTILS_MAP_H
#define C_FALCONUTILS_MAP_H

#include "../SimplefiedStrings/SimplefiedStrings.h"
#include "../ArrayList/ArrayList.h"

typedef struct MapPrototype Map;


Map* MAP_new();

void MAP_init(Map* map, size_t valueTypeSize);

void MAP_free(Map* map);

void MAP_add(Map* map, char* key, void* item);

void MAP_add_stringKey(Map* map, String* key, void* item);

void MAP_remove(Map* map, char* key);

void MAP_remove_stringKey(Map* map, String* key);

void* MAP_get(Map* map, char* key);

void* MAP_get_stringKey(Map* map, String* key);

ArrayList* MAP_getValues(Map* map);

ArrayList* MAP_getKeys(Map* map);

size_t MAP_getSizeOf();

#endif //C_FALCONUTILS_MAP_H
