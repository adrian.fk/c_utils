//
// Created by Adrian Karlsen on 21/03/2021.
//

#include "Hash.h"

long slHash(String* input) {
    return calHash(STRING_getCharArr(input));
}

int siHash(String* input) {
    return caiHash(STRING_getCharArr(input));
}

long calHash(char* input) {
    unsigned long hashVal;

    for (hashVal = 0; *input != '\0'; input++)
        hashVal = *input + 31 * hashVal;
    return hashVal;
}

int caiHash(char* input) {
    unsigned hashVal;

    for (hashVal = 0; *input != '\0'; input++)
        hashVal = *input + 31 * hashVal;
    return hashVal;
}