//
// Created by Adrian Karlsen on 21/03/2021.
//

#ifndef C_FALCONUTILS_HASH_H
#define C_FALCONUTILS_HASH_H

#include "./../../../Utils.h"

long slHash(String* input);

int siHash(String* input);

long calHash(char* input);

int caiHash(char* input);

#endif //C_FALCONUTILS_HASH_H
