//
// Created by Adrian Karlsen on 21/03/2021.
//

#include "../HashMap.h"
#include <limits.h>

#define HASHMAP_NO_INPUT -33


struct Class_HashMap {
    int hash_size;
    size_t data_size;

    Array keys;
    int* hash_to_pos_map;
    Array data;
};


HashMap* HASHMAP_new(size_t elementSize) {
    HashMap* map = (HashMap*) malloc(sizeof(struct Class_HashMap));

    if (map) {
        String* dummyHashString = STRING_init("baseSize");
        map->hash_size = HASHMAP_getKeyHash(dummyHashString);
        map->data_size = elementSize;
        map->hash_to_pos_map = (int*) malloc(map->hash_size * sizeof(int));
        map->data = ARRAY_constructor(elementSize);
        map->keys = ARRAY_constructor(STRING_getSizeOf());

        STRING_free(dummyHashString);
    }

    return map;
}

void HASHMAP_free(HashMap* map) {
    free(map->hash_to_pos_map);
    ARRAY_destroy(map->keys);
    ARRAY_destroy(map->data);
    free(map);
}

size_t HASHMAP_getSizeOf() {
    return sizeof(struct Class_HashMap);
}

int HASHMAP_getKeyHash(String* key) {
    return siHash(key);
}


/**
 * @objective: to expand the allocated initialSize of the key map array
 * @param map
 * @return {Boolean} Boolean value representing whether the realloc could be carried out successfully.
 */
Boolean HASHMAP_expand(HashMap* map, int newHashSize) {
    if (newHashSize < INT_MAX) {
        map->hash_to_pos_map = realloc(map->hash_to_pos_map, newHashSize * sizeof(int));
        if (map->hash_to_pos_map) {
            map->hash_size = newHashSize;
            return TRUE;
        }
    }
    return FALSE;
}

void HASHMAP_add(HashMap* map, char* key, void* element) {
    String* key_string = STRING_init(key);
    HASHMAP_add_useStringKey(map, key_string, element);
}

void HASHMAP_add_useStringKey(HashMap* map, String* key, void* element) {
    int index = HASHMAP_getKeyHash(key);

    if (index > map->hash_size) HASHMAP_expand(map, index);

    ARRAY_add(map->keys, key);
    map->hash_to_pos_map[index] = ARRAY_getLength(map->data);
    ARRAY_add(map->data, element);
}

void HASHMAP_remove(HashMap* map, char* key) {
    String* key_string = STRING_init(key);
    HASHMAP_remove_useStringKey(map, key_string);
}

void HASHMAP_remove_useStringKey(HashMap* map, String* key) {
    unsigned int keyHash = HASHMAP_getKeyHash(key);
    ARRAY_remove(map->keys, map->hash_to_pos_map[keyHash]);
    ARRAY_remove(map->data, map->hash_to_pos_map[keyHash]);
    map->hash_to_pos_map[keyHash] = HASHMAP_NO_INPUT;
}

void* HASHMAP_get(HashMap* map, char* key) {
    String* string_key = STRING_init(key);
    return HASHMAP_get_useStringKey(map, string_key);
}

void* HASHMAP_get_useStringKey(HashMap* map, String* key) {
    if (HASHMAP_getKeyHash(key) <= map->hash_size) {
        int index = map->hash_to_pos_map[HASHMAP_getKeyHash(key)];
        return ARRAY_get(map->data, index);
    }
    return NULL;
}

void* HASHMAP_set(HashMap* map, char* key, void* elem) {
    String* string_key = STRING_init(key);
    return HASHMAP_set_useStringKey(map, string_key, elem);
}

void* HASHMAP_set_useStringKey(HashMap* map, String* key, void* elem) {
    if (HASHMAP_getKeyHash(key) < map->hash_size) {
        int index = map->hash_to_pos_map[HASHMAP_getKeyHash(key)];
        if (index != HASHMAP_NO_INPUT && index < ARRAY_getLength(map->data)) {
            ARRAY_set(map->keys, index, key);
            ARRAY_set(map->data, index, elem);
            return ARRAY_get(map->data, index);
        }
        else {
            HASHMAP_add_useStringKey(map, key, elem);
            return ARRAY_get(map->data, index);
        }
    }
    return NULL;
}

size_t HASHMAP_getElmSize(HashMap* map) {
    return map->data_size;
}

Array HASHMAP_getValues(HashMap* map) {
    return map->data;
}

Array HASHMAP_getKeys(HashMap* map) {
    return map->keys;
}


