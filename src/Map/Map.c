//
// Created by Adrian Karlsen on 31/01/2021.
//

#include "Map.h"

struct MapPrototype {
    ArrayList* keys;
    ArrayList* values;
};


void* MAP_get(Map* map, char* key);
void MAP_add_stringKey(Map* map, String* key, void* item);
void* MAP_get_stringKey(Map* map, String* key);



Map* MAP_new() {
    Map* m = (Map*) malloc(sizeof(struct MapPrototype));
    if (m != NULL) {
        m->keys = ARRAY_LIST_new();
        m->values = ARRAY_LIST_new();
    }
    return m;
}

void MAP_init(Map* map, size_t valueTypeSize) {
    if (NULL != map) {
        ARRAY_LIST_init(map->values, valueTypeSize);
        ARRAY_LIST_init(map->keys, STRING_getSizeOf());
    }
}

void MAP_free(Map* map) {
    if (NULL != map) {
        ARRAY_LIST_free(map->keys);
        ARRAY_LIST_free(map->values);
        free(map);
    }
}

int locateKeyIndexStringKey(Map* map, String* key) {
    for (int i = 0; i < ARRAY_LIST_length(map->keys); ++i) {
        if (STRING_equals(key, (String*) ARRAY_LIST_get(map->keys, i)))
            return i;
    }
    return -1;
}

void MAP_add(Map* map, char* key, void* item) {
    if (map != NULL) {
        String* keyString = STRING_new();
        STRING_set(keyString, key);
        MAP_add_stringKey(map, keyString, item);
        //STRING_destroy(keyString);
    }
}

void MAP_add_stringKey(Map* map, String* key, void* item) {
    if (map != NULL) {
        int  containsKey = locateKeyIndexStringKey(map, key);
        if (containsKey == -1) {
            ARRAY_LIST_add(map->keys, key);
            ARRAY_LIST_add(map->values, item);
        }
    }
}

void MAP_remove(Map* map, char* inputKey) {
    String* key = STRING_new();
    STRING_set(key, inputKey);
    MAP_remove_stringKey(map, key);
    STRING_destroy(key);
}

void MAP_remove_stringKey(Map* map, String* key) {
    if (map != NULL) {
        int index = locateKeyIndexStringKey(map, key);
        if (index >= 0) {
            ARRAY_LIST_remove(map->keys, index);
            ARRAY_LIST_remove(map->values, index);
        }
    }
}

void* MAP_get(Map* map, char* key) {
    void* out = NULL;
    if (map != NULL) {
        String* keyString = STRING_new();
        STRING_set(keyString, key);
        out = MAP_get_stringKey(map, keyString);
        STRING_destroy(keyString);
    }
    return out;
}

void* MAP_get_stringKey(Map* map, String* key) {
    int keyIndex = locateKeyIndexStringKey(map, key);
    if (keyIndex >= 0) {
        return ARRAY_LIST_get(map->values, keyIndex);
    }
    return NULL;
}

ArrayList* MAP_getValues(Map* map) {
    return map->values;
}

ArrayList* MAP_getKeys(Map* map) {
    return map->keys;
}

size_t MAP_getSizeOf() {
    return sizeof(struct MapPrototype);
}
