//
// Created by Adrian Karlsen on 21/01/2021.
//

#include "../../SimpleMenuEntryAdapter.h"


void SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToMenuEntry(SimpleMenuEntry* _dest,
                                                            String* id,
                                                            String* text,
                                                            void (*executionFunctionCall)) {

    SIMPLE_MENU_ENTRY_setId(_dest, id);
    SIMPLE_MENU_ENTRY_setText(_dest, text);
    SIMPLE_MENU_ENTRY_setExecutionFunction(_dest, executionFunctionCall);
}


SimpleMenuEntry* SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToNewMenuEntry(String* id,
                                                                           String* text,
                                                                           void (*executionFunctionCall)) {

    SimpleMenuEntry* entry = SIMPLE_MENU_ENTRY_init();

    SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToMenuEntry(entry,
                                                           id,
                                                           text,
                                                           executionFunctionCall);

    return entry;
}
