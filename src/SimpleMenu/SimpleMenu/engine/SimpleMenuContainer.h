//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUCONTAINER_H
#define C_SIMPLEMENU_SIMPLEMENUCONTAINER_H

#include "SimpleMenuEntry.h"

typedef struct SimpleMenuContainer__prototype SimpleMenuContainer;

SimpleMenuContainer* SIMPLE_MENU_CONTAINER_init();

void SIMPLE_MENU_CONTAINER_addMenuEntry(SimpleMenuContainer* containerObj, SimpleMenuEntry* entry);

void SIMPLE_MENU_CONTAINER_removeEntryById(SimpleMenuContainer* container, String* id);

void SIMPLE_MENU_CONTAINER_removeEntryByIndex(SimpleMenuContainer* container, int index);

int SIMPLE_MENU_CONTAINER_containerLength(SimpleMenuContainer* container);

SimpleMenuEntry* SIMPLE_MENU_CONTAINER_get(const SimpleMenuContainer* containerObj, int index);

SimpleMenuEntry* SIMPLE_MENU_CONTAINER_getById(const SimpleMenuContainer* containerObj, String* id);

String* SIMPLE_MENU_CONTAINER_getMenuTitle(SimpleMenuContainer* container);

void SIMPLE_MENU_CONTAINER_setMenuTitle(SimpleMenuContainer* container, String* title);

Boolean SIMPLE_MENU_CONTAINER_containsId(SimpleMenuContainer* container, String* id);

int SIMPLE_MENU_CONTAINER_getLongestEntryLength(SimpleMenuContainer* container);

void SIMPLE_MENU_CONTAINER_setExitStatement(SimpleMenuContainer* container, String* exitStatement);

String* SIMPLE_MENU_CONTAINER_getExitStatement(const SimpleMenuContainer* container);

void SIMPLE_MENU_CONTAINER_setErrorMsg(SimpleMenuContainer* container, String* errorMsg);

String* SIMPLE_MENU_CONTAINER_getErrorMsg(SimpleMenuContainer* container);

void SIMPLE_MENU_CONTAINER_destroy(SimpleMenuContainer* container);

#endif //C_SIMPLEMENU_SIMPLEMENUCONTAINER_H
