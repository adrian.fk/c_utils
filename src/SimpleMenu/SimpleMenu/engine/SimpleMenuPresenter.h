//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUPRESENTER_H
#define C_SIMPLEMENU_SIMPLEMENUPRESENTER_H

#include "SimpleMenuContainer.h"
#include "Presenter/SimpleMenuConsoleView.h"
#include "SimpleMenuContainer.h"
#include "Presenter/SimpleMenuViewModel.h"
#include "Presenter/SimpleMenuViewModelAdapter.h"

#define MODE_DEFAULT 0
#define MODE_CONSOLE 1
#define MODE_MAX_VALUE MODE_CONSOLE

typedef int Mode;

typedef struct SimpleMenuPresenterPrototype SimpleMenuPresenter;


SimpleMenuPresenter* SIMPLE_MENU_PRESENTER_init();

void SIMPLE_MENU_PRESENTER_setMode(SimpleMenuPresenter* presenter, Mode mode);

void SIMPLE_MENU_PRESENTER_setMenuTitle(SimpleMenuPresenter* presenter, String* title);

void SIMPLE_MENU_PRESENTER_presentMenu(SimpleMenuPresenter* presenter, SimpleMenuContainer* menuContainer, const Bool* error);

void SIMPLE_MENU_PRESENTER_destroy(SimpleMenuPresenter* presenter);

String* SIMPLE_MENU_PRESENTER_getInput();

#endif //C_SIMPLEMENU_SIMPLEMENUPRESENTER_H
