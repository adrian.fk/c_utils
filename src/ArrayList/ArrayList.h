//
// Created by Adrian Karlsen on 30/01/2021.
//

#ifndef C_FALCONUTILS_ARRAYLIST_H
#define C_FALCONUTILS_ARRAYLIST_H

#include "../LinkedList/LinkedList.h"

typedef struct ArrayListPrototype ArrayList;

ArrayList* ARRAY_LIST_new();

void ARRAY_LIST_init(ArrayList* arrayList, int elementSize);

void ARRAY_LIST_free(ArrayList* arrayList);

void ARRAY_LIST_add(ArrayList* arrayList, void* element);

void* ARRAY_LIST_get(ArrayList* arrayList, int index);

void* ARRAY_LIST_remove(ArrayList* arrayList, int index);

int ARRAY_LIST_length(ArrayList* arrayList);

void ARRAY_LIST_forEach(ArrayList* arrayList, void (*functionCallForEachItem(void*)));

size_t ARRAY_LIST_getSizeOf();



#endif //C_FALCONUTILS_ARRAYLIST_H
