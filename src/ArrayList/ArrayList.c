//
// Created by Adrian Karlsen on 30/01/2021.
//

#include <stdlib.h>
#include "ArrayList.h"

struct ArrayListPrototype {
    LinkedList* list;
};


ArrayList* ARRAY_LIST_new() {
    ArrayList* list = (ArrayList*) malloc (sizeof(struct ArrayListPrototype));
    if (NULL != list) {
        list->list = LINKED_LIST_new();
    }
    return list;
}

void ARRAY_LIST_init(ArrayList* arrayList, int elementSize) {
    LINKED_LIST_init(arrayList->list, elementSize);
}

void ARRAY_LIST_free(ArrayList* arrayList) {
    LINKED_LIST_free(arrayList->list);
    free(arrayList->list);
    free(arrayList);
}

void ARRAY_LIST_add(ArrayList* arrayList, void* element) {
    LINKED_LIST_push(arrayList->list, element);
}

void* ARRAY_LIST_get(ArrayList* arrayList, int index) {
    return LINKED_LIST_get(arrayList->list, index);
}

void* ARRAY_LIST_remove(ArrayList* arrayList, int index) {
    LINKED_LIST_removeIndx(arrayList->list, index);
}

int ARRAY_LIST_length(ArrayList* arrayList) {
    return LINKED_LIST_getLength(arrayList->list);
}

void ARRAY_LIST_forEach(ArrayList* arrayList, void (*functionCallForEachItem(void*))) {
    int i = 0;
    void* target = LINKED_LIST_get(arrayList->list, i++);
    do {
        functionCallForEachItem(target);
        target = LINKED_LIST_get(arrayList->list, i++);
    } while (target != NULL);
}

size_t ARRAY_LIST_getSizeOf() {
    return sizeof(struct ArrayListPrototype);
}
