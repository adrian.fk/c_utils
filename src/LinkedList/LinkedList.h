//
// Created by Adrian Karlsen on 29/01/2021.
//

#ifndef C_FALCONUTILS_LINKEDLIST_H
#define C_FALCONUTILS_LINKEDLIST_H

#include <stdlib.h>
#include <stdio.h>
#include "../SimplefiedStrings/SimplefiedStrings.h"


typedef struct LinkedListPrototype  LinkedList;


LinkedList* LINKED_LIST_new();

void LINKED_LIST_free(LinkedList* linkedList);

void LINKED_LIST_init(LinkedList* linkedList, size_t elementSize);

void LINKED_LIST_push(LinkedList* linkedList, void* item);

void LINKED_LIST_prepend(LinkedList* linkedList, void* item);

void LINKED_LIST_removeIndx(LinkedList* linkedList, int index);

int LINKED_LIST_isEmpty(LinkedList* linkedList);

int LINKED_LIST_getLength(LinkedList* linkedList);

int LINKED_LIST_length(LinkedList* linkedList);

void* LINKED_LIST_pop(LinkedList* linkedList);

void* LINKED_LIST_get(LinkedList* linkedList, int tarIndex);

#endif //C_FALCONUTILS_LINKEDLIST_H
