//
// Created by Adrian Karlsen on 29/01/2021.
//

#include <string.h>
#include "LinkedList.h"


////////////////////////////    PRE-PROCESS MACROS   ////////////////////////////
#define DEFAULT_ELEMENT_SIZE 64




////////////////////////////    DATA STRUCTURES   ////////////////////////////
typedef struct Node {
    int elementSize;
    void* element;
    struct Node* next;
    struct Node* prev;
}Node;

struct LinkedListPrototype {
    size_t elementSize;
    int length;
    Node* head;
    Node* tail;
    Node* poi;
    int indexPoi;
};


////////////////////////////    FUNCTION DECLARATIONS   ////////////////////////////
Node* LINKED_LIST_Node_create(void* item, size_t itemSize, Node* prev, Node* next);
void LINKED_LIST_Node_free(Node* node);
void LINKED_LIST_Node_removeLinkedNode(Node* node);
void* LINKED_LIST_getAndUnlink(LinkedList* linkedList, int index);
Node *LINKED_LIST_Node_get(LinkedList *ll, int tarIndex);




LinkedList* LINKED_LIST_new() {
    LinkedList* ll = (LinkedList*) malloc(sizeof(struct LinkedListPrototype));
    if (NULL != ll) {
        ll->head = NULL;
        ll->tail = NULL;
        ll->elementSize = DEFAULT_ELEMENT_SIZE;
        ll->length = 0;
        ll->indexPoi = -1;
    }

    return ll;
}

void LINKED_LIST_free(LinkedList* linkedList) {
    if (linkedList->length > 0) {
        Node* target = linkedList->head;
        while (target != NULL) {
            Node* tmp = target;
            target = target->next;
            LINKED_LIST_Node_free(tmp);
        }
    }
    free(linkedList);
}

void LINKED_LIST_init(LinkedList* linkedList, size_t elementSize) {
    if (NULL != linkedList) linkedList->elementSize = elementSize;
}

void LINKED_LIST_createFirstNode(LinkedList *linkedList, void *item) {
    Node* node = LINKED_LIST_Node_create(item, linkedList->elementSize, NULL, NULL);
    linkedList->head = node;
    linkedList->tail = node;
}

void LINKED_LIST_createAltNodeAtTail(LinkedList *linkedList, void *item) {
    Node* node = LINKED_LIST_Node_create(item, linkedList->elementSize, linkedList->tail, NULL);
    linkedList->tail->next = node;
    linkedList->tail = node;
}

void LINKED_LIST_createAltNodeAtHead(LinkedList *linkedList, void *item) {
    Node* node = LINKED_LIST_Node_create(item, linkedList->elementSize, NULL, linkedList->head);
    linkedList->head->prev = node;
    linkedList->head = node;
}

void LINKED_LIST_push(LinkedList* linkedList, void* item) {
    if (NULL != linkedList) {
        if (LINKED_LIST_isEmpty(linkedList)) {
            LINKED_LIST_createFirstNode(linkedList, item);
        }
        else {
            LINKED_LIST_createAltNodeAtTail(linkedList, item);
        }
        linkedList->length++;
    }
}

void LINKED_LIST_prepend(LinkedList* linkedList, void* item) {
    if (NULL != linkedList) {
        if (LINKED_LIST_isEmpty(linkedList)) {
            LINKED_LIST_createFirstNode(linkedList, item);
        }
        else {
            LINKED_LIST_createAltNodeAtHead(linkedList, item);
        }
        linkedList->length++;
    }
}

void LINKED_LIST_removeIndx(LinkedList* linkedList, int index) {
    if (index < LINKED_LIST_getLength(linkedList)) {
        Node* tar = LINKED_LIST_Node_get(linkedList, index);

        if (index == linkedList->length - 1) {
            linkedList->tail = tar->prev;
            if (tar->prev) tar->prev->next = NULL;
        }

        LINKED_LIST_Node_removeLinkedNode(tar);

        linkedList->length--;
        if (linkedList->length == 0) {
            linkedList->head = NULL;
            linkedList->tail = NULL;
        }
    }
    //LINKED_LIST_Node_free(LINKED_LIST_getAndUnlink(linkedList, index));
}

//TODO implement set method

int LINKED_LIST_isEmpty(LinkedList* linkedList) {
    if (linkedList->head == NULL || linkedList->length <= 0) return 1;
    return 0;
}


int LINKED_LIST_getLength(LinkedList* linkedList) {
    return linkedList->length;
}

int LINKED_LIST_length(LinkedList* linkedList) {
    int len = 0;
    Node* tar = linkedList->head;
    while (NULL != tar) {
        len++;
        tar = tar->next;
    }
    linkedList->length = len;
    return len;
}


Node* LINKED_LIST_Node_create(void* item, size_t itemSize, Node* prev, Node* next) {
    Node* node = (Node*) malloc(sizeof (struct Node));
    if (NULL != node) {
        node->elementSize = itemSize;
        node->element = malloc(itemSize);
        if (NULL != node->element) memcpy(node->element, item, itemSize);
        node->next = next;
        node->prev = prev;
    }
    return node;
}

void LINKED_LIST_Node_setPrev(Node* node, Node* prev) {
    if (NULL != node) node->prev = prev;
}

void LINKED_LIST_Node_setNext(Node* node, Node* next) {
    if (NULL != node) node->next = next;
}

void* LINKED_LIST_pop(LinkedList* linkedList) {
    return LINKED_LIST_getAndUnlink(linkedList, 0);
}

int LINKED_LIST_Node_getIndex(LinkedList* linkedList, Node* node) {
    int index = 0;
    Node* tar = linkedList->head->next;
    while (tar != NULL) {
        if (tar->prev->next == node) return index;

        index++;
        tar = tar->next;
    }
    linkedList->poi = tar;
    linkedList->indexPoi = index;
    return index;
}

void LINKED_LIST_Node_removeLinkedNode(Node* node) {
    Node* next = node->next;
    Node* prev = node->prev;

    if (prev) prev->next = next;
    if (next) next->prev = prev;

    LINKED_LIST_Node_free(node);
}

void LINKED_LIST_Node_removeNode(Node* node) {
    LINKED_LIST_Node_free(node);
}

void LINKED_LIST_Node_free(Node* node) {
    free(node->element);
    free(node);
}

void* LINKED_LIST_get(LinkedList* linkedList, int tarIndex) {
    Node* target = LINKED_LIST_Node_get(linkedList, tarIndex);
    if (NULL != target) return target->element;
    return NULL;
}

Node *LINKED_LIST_Node_get(LinkedList *ll, int tarIndex) {
    if (tarIndex < ll->length && tarIndex >= 0) {
        if (tarIndex == ll->indexPoi) {
            return ll->poi;
        }
        Node* target = ll->head;
        for (int i = 0; i <= tarIndex; ++i) {
            if (i == tarIndex) {
                ll->indexPoi = 1;
                ll->poi = target;
                return target;
            }
            target = target->next;
        }
    }
    return NULL;
}

void* LINKED_LIST_getAndUnlink(LinkedList* linkedList, int index) {
    Node* target = LINKED_LIST_Node_get(linkedList, index);
    if (NULL != target) {
        //Unlink
        Node* prev = target->prev;
        Node* next = target->next;

        prev->next = next;
        next->prev = prev;

        //TODO take into account head and tail  -   from length?
        //TODO linkedlist.length--
    }
    return target->element;
}

